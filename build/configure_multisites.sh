#!/bin/bash

PAYMENT_PROVIDERS=(gcen goji lemonway mangopay offline)
REDIS_DATABASE=2
cp configs/supervisord.conf configs/supervisord.conf_SAMPLE
cp configs/crontab configs/crontab_SAMPLE
for PAYMENT_PROVIDER in ${PAYMENT_PROVIDERS[*]}
do
    APP_ADMIN_URL=$(cat src/.env | grep -e ^APP_ADMIN_URL | sed 's%://%://'$PAYMENT_PROVIDER.'%')
    echo $APP_ADMIN_URL
    APP_API_URL=$(cat src/.env | grep -e ^APP_API_URL | sed 's%://%://'$PAYMENT_PROVIDER.'%')
    echo $APP_API_URL
    APP_FRONTEND_URL=$(cat src/.env | grep -e ^APP_FRONTEND_URL | sed 's%://%://'$PAYMENT_PROVIDER.'%')
    echo $APP_FRONTEND_URL
    WEBAPP_API_URL=$(cat src/.env | grep -e ^WEBAPP_API_URL | sed 's%://%://'$PAYMENT_PROVIDER.'%')
    echo $WEBAPP_API_URL
    if [ -f "runtime/$PAYMENT_PROVIDER-installed" ]
        then
            MODULE_PARAMETER="MODULE_$(echo ${PAYMENT_PROVIDER^^})_ENABLED"
            echo $MODULE_PARAMETER
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan multisite:add $PAYMENT_PROVIDER"
            sed 's%APP_ADMIN_URL=.*%'$APP_ADMIN_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%APP_API_URL=.*%'$APP_API_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%APP_FRONTEND_URL=.*%'$APP_FRONTEND_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%WEBAPP_API_URL=.*%'$WEBAPP_API_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%DB_PREFIX=.*%DB_PREFIX='$PAYMENT_PROVIDER'_%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%PAYMENT_PROVIDER=.*%PAYMENT_PROVIDER='$PAYMENT_PROVIDER'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%KYC_PROVIDER=.*%KYC_PROVIDER='$PAYMENT_PROVIDER'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%'$MODULE_PARAMETER'=.*%'$MODULE_PARAMETER'=1%' -i src/.env.$PAYMENT_PROVIDER
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan migrate --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan cache:clear --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan view:clear --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan config:clear --env=$PAYMENT_PROVIDER && php artisan route:clear --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php admin/artisan config:clear --env=$PAYMENT_PROVIDER && php admin/artisan route:clear --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan permissions:sync --env=$PAYMENT_PROVIDER"
        else
            MODULE_PARAMETER="MODULE_$(echo ${PAYMENT_PROVIDER^^})_ENABLED"
            echo $MODULE_PARAMETER
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan multisite:add $PAYMENT_PROVIDER"
            sed 's%APP_ADMIN_URL=.*%'$APP_ADMIN_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%APP_API_URL=.*%'$APP_API_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%APP_FRONTEND_URL=.*%'$APP_FRONTEND_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%WEBAPP_API_URL=.*%'$WEBAPP_API_URL'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%DB_PREFIX=.*%DB_PREFIX='$PAYMENT_PROVIDER'_%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%PAYMENT_PROVIDER=.*%PAYMENT_PROVIDER='$PAYMENT_PROVIDER'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%KYC_PROVIDER=.*%KYC_PROVIDER='$PAYMENT_PROVIDER'%' -i src/.env.$PAYMENT_PROVIDER
            sed 's%'$MODULE_PARAMETER'=.*%'$MODULE_PARAMETER'=1%' -i src/.env.$PAYMENT_PROVIDER
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan migrate --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan migrate --seed --env=$PAYMENT_PROVIDER"
            docker-compose exec -T --privileged --index=1 app bash -c "php artisan permissions:sync --env=$PAYMENT_PROVIDER"
            touch runtime/$PAYMENT_PROVIDER-installed
    fi
    REDIS_DATABASE=$[REDIS_DATABASE+1]
    sed 's?REDIS_DATABASE=.*?REDIS_DATABASE='$REDIS_DATABASE'?' -i src/.env.$PAYMENT_PROVIDER

    ####################
    #### SUPERVISOR ####
    ####################

    cp configs/supervisord.conf_SAMPLE configs/supervisord.conf_$PAYMENT_PROVIDER
    sed 's%\[program:laravel-lenderkit-queue\]%\[program:laravel-lenderkit-queue-'$PAYMENT_PROVIDER'\]%' -i configs/supervisord.conf_$PAYMENT_PROVIDER
    sed 's%--tries=2%--tries=2 --env='$PAYMENT_PROVIDER'%' -i configs/supervisord.conf_$PAYMENT_PROVIDER
    sed 's%supervisord.log%supervisord-'$PAYMENT_PROVIDER'.log%' -i configs/supervisord.conf_$PAYMENT_PROVIDER
    echo "" >> configs/supervisord.conf
    cat configs/supervisord.conf_$PAYMENT_PROVIDER >> configs/supervisord.conf
    rm configs/supervisord.conf_$PAYMENT_PROVIDER

    #####################
    ###### CRONTAB ######
    #####################
    cp configs/crontab_SAMPLE configs/crontab_$PAYMENT_PROVIDER
    sed 's%schedule:run%schedule:run --env='$PAYMENT_PROVIDER'%' -i configs/crontab_$PAYMENT_PROVIDER
    sed 's%cron-scheduler%'$PAYMENT_PROVIDER'-cron-scheduler%' -i configs/crontab_$PAYMENT_PROVIDER
    echo "" >> configs/crontab
    cat configs/crontab_$PAYMENT_PROVIDER >> configs/crontab
    rm configs/crontab_$PAYMENT_PROVIDER

done

echo "" >> configs/crontab
echo "" >> configs/supervisord.conf
rm configs/supervisord.conf_SAMPLE
chown -R www-data src/admin/bootstrap
chown -R www-data src/api/bootstrap
chown -R www-data src/storage

make stop ||true
make run
