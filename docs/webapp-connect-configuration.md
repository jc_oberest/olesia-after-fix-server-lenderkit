# WebApp Connection

The application can be connected with the WebApp. There is the node-js server on the WebApp side, that has 2 endpoints
 for theme rebuilding: `/app/rebuild` - used just for rebuilding and `/theme` - used for a new theme config upload.   

## Configurations

In the application-level `.env` file you need to specify 2 parameters: `WEBAPP_API_URL` and `WEBAPP_API_TOKEN`. Both
 of them can be provided by the FE developer.
 
`WEBAPP_API_URL` - Base URL for all the WebApp server endpoints.

`WEBAPP_API_TOKEN` - Actually it's the value of the `X-Csrf-Token` header, used by WebApp server to authorize
 requests. Get it from the FE dev.

## Endpoints

`POST /app/rebuild` - Empty body. Just runs `npm run build` on the WebApp side.

`PATCH /theme` - Upload a new `theme-config.json` to the WebApp and rebuild it. Theme config file is being generated
 on the basis of `ThemeResource`. 
 
 Example theme config looks like this:
 
```js
export default {
  "id": 1,
  "name": "classic",
  "title": "Classic Preview",
  "active": false,
  "assets": {
    "css": {
      "styles": "{domain}/css/theme-dark.css"
    },
    "images": {
      "logo_light": "{domain}/images/logo-default",
      "logo_dark": "{domain}/images/logo-default-inversed.svg",
      "logo_icon": "{domain}/images/logo-default-small.svg"
    }
  },
  "variables": {
    "color_scheme": "dark",
    "heading_font_family": "Verdana",
    "heading_font_h1_size": 40,
    "heading_font_h1_style": "italic"
  }
}
```

## Local setup

To set up the WebApp locally, clone [this repo](https://bitbucket.org/justcoded/lenderkit-webapp-starter/src/master) and
follow [this guide](https://bitbucket.org/justcoded/lenderkit-webapp-starter/src/master/README.md).

### Configuration

By default, the WebApp API port is `8081`. This setting can be modified in `<webapp-root>/.env`, via the
 `HOST_API_PORT` parameter. 
 
LK connects to the WebApp inside the docker network, so we need to specify the WebApp API IP address as the
 `WEBAPP_API_URL` parameter.
 
If you are a Mac user, then go directly to the [Mac OS](#mac-os) section.

#### Linux

In order to get the WebApp API IP, run the following commands:

```bash
docker ps
```
This one will provide you with the list of containers being run. Find there a row containing 
something like `"webapp-api"`, and copy the value either from the `"NAMES"` column or from 
the `"CONTAINER ID"` (preferred) one.

Then run

```bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' %container_id%
```

Just replace `%containder_id%` with a real container ID, gotten from the previous step.

#### Mac OS

On Mac, you work under the Vagrant VM, so the easiest way to get the IP of the WebApp API is to open `<root>/.env`
and look for `XDEBUG_REMOTE_HOST`. On Mac machines it will be automatically set to a correct host IP address, after
running the `make run` command.

#### Set the .env value

The result should be something like `172.31.0.7`. We will use this value to specify the `WEBAPP_API_URL` parameter in 
the `<app-root>/.env` file.

```dotenv
WEBAPP_API_URL=http://172.31.0.7:8081
``` 

### API token

API token, which must be provided in the `X-Csrf-Token` header, can be found in `<webapp-root>/api/.env`. 
Look for the `WEBAPP_SERVER_TOKEN` parameter. 

Then we specify this one in our `<app-root>/.env` file, the parameter for it is `WEBAPP_API_TOKEN`.

```dotenv
WEBAPP_API_TOKEN=1sXFUBaTSLGr1GRl
```


