# Running Unit tests

## Init test database

To run unit tests you need to create a test database before that. If you use standard `db` container, you can do this
OUTSIDE php container (on **root level**) by running `make phpunit-init`.

This command will create a database `lenderkit_test`. After that you can run phpunit commands inside php container.

## Running once

If you want to run test once and don't want to debug unit tests, you can use `make test`.

This command will run several commands inside:

* `make phpunit-init` - init test database
* `make phpunit` - run Core testsuite
* `make phpunit-clean` - remove test database

