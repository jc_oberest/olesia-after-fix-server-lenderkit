<?php

declare(strict_types=1);

namespace Api\Http\Controllers;

use LenderKit\Api\Http\Controllers\ApiController;

/**
 * Class Controller
 *
 * @package App\Api\Controllers
 */
abstract class Controller extends ApiController
{

}
