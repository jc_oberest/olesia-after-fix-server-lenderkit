<?php

declare(strict_types=1);

namespace Api\Forms;

use LenderKit\Api\Forms\ApiForm as LenderKitApiForm;

/**
 * Class Form
 *
 * @package Api\Forms
 */
abstract class ApiForm extends LenderKitApiForm
{

}
