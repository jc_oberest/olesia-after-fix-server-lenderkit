<?php

declare(strict_types=1);

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends \LenderKit\Database\Seeds\DatabaseSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->seedCore();
        $this->seedModules();
    }

    /**
     * Seed data from Modules
     */
    protected function seedModules()
    {
        // all the module seeds should be added to makefile and surrounded with module identifier lines.
        // seeder class can be added here only on custom commercial development.
    }
}
