<?php

declare(strict_types=1);

use LenderKit\Database\Seeds\{BankAccountsTableSeeder,
    OfferingsTableSeeder,
    TransactionTableSeeder,
    UserRecalculation,
    UsersTableSeeder,
    WalletTableSeeder};

/**
 * Class DatabaseSeeder
 */
class TestDataSeeder extends \LenderKit\Database\Seeds\Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->disableActivityLogs();

        $this->call(PoliciesSeeder::class);

//        $this->call(UsersTableSeeder::class);
//        $this->call(OfferingsTableSeeder::class);
//        $this->call(WalletTableSeeder::class);
//        $this->call(BankAccountsTableSeeder::class);
//        $this->call(TransactionTableSeeder::class);
//        $this->call(UserRecalculation::class);
    }
}
